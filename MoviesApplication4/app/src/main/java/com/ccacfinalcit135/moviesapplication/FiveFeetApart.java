package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class FiveFeetApart extends AppCompatActivity { //open public class
    private AnimationDrawable FiveAnimation; //define variable
    private ImageView FiveFeetImage; //define variable
    private Button FiveBtn; //define variable
    @Override
    protected void onCreate(Bundle savedInstanceState) { // open void oncreate
        super.onCreate( savedInstanceState ); // create instance
        setContentView( R.layout.activity_five_feet_apart ); //set activity
        FiveBtn =   (Button) findViewById( R.id.btnFiveID ); //define variable

        FiveFeetImage = (ImageView)findViewById(R.id.FiveImage); //define variable
        FiveFeetImage.setBackgroundResource(R.drawable.five_feet_apart); // set background image
        FiveAnimation=(AnimationDrawable) FiveFeetImage.getBackground(); // set background

        FiveAnimation.start(); //start activity

        Handler myHandler = new Handler(); //define handler
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() { //start handler
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.alita_anim); //define variable
                FiveFeetImage.startAnimation(startanimation); //start activity
            }
        }, 25); // delay slight
        FiveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { // open onclicklistener
                Intent browserIntent = new Intent( // load website
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/five-feet-apart-215555/movie-times"));
                startActivity(browserIntent); //start browser
                Toast.makeText(FiveFeetApart.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); //notify user
            }
        }); // close onclicklistener
    }// close void onclick
} //close public class
