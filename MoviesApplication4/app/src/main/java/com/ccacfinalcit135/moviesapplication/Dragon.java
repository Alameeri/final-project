package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Dragon extends AppCompatActivity { // open public class
    private AnimationDrawable DragonAnimation; //define variable
    private ImageView DragonImage; //define variable
    private Button DragonBtn; //define variable

    @Override
    protected void onCreate(Bundle savedInstanceState) { // open void oncreate
        super.onCreate( savedInstanceState ); // open instance
        setContentView( R.layout.activity_dragon ); //set activity
        DragonBtn =   (Button) findViewById( R.id.btnDragonID ); //define variable

        DragonImage = (ImageView)findViewById(R.id.DragonImage); //define variable
        DragonImage.setBackgroundResource(R.drawable.how_to_train_your_dragon_the_hidden_world); //set background image
        DragonAnimation=(AnimationDrawable) DragonImage.getBackground(); //define variable

        DragonAnimation.start(); //start activity

        Handler myHandler = new Handler(); // define handler
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() { //start handler
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.dragon_anim); //define variable
                DragonImage.startAnimation(startanimation);  //start activity
            }
        }, 25); //delay slight
        DragonBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { // open onlick
                Intent browserIntent = new Intent( //load website
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/how-to-train-your-dragon-the-hidden-world-212232/movie-times"));
                startActivity(browserIntent);
                Toast.makeText(Dragon.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); //show message to user
            }
        }); //close onclick
    } //close void oncreate
} //close public class
