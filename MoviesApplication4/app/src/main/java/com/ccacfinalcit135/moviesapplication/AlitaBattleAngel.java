package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class AlitaBattleAngel extends AppCompatActivity { // open public class
    private AnimationDrawable AlitaAnimation; //define variable
    private ImageView AlitaImage; //define variable
    private Button AlitaBtn;//define variable


    @Override
    protected void onCreate(Bundle savedInstanceState) { // open void
        super.onCreate( savedInstanceState ); //create instance state
        setContentView( R.layout.activity_alita_battle_angel ); //set as front
        AlitaBtn =   (Button) findViewById( R.id.btnAlitaID ); //define variable

        AlitaImage = (ImageView)findViewById(R.id.AlitaID); //define variable
       AlitaImage.setBackgroundResource(R.drawable.alita_battle_angel); // set background image
        AlitaAnimation=(AnimationDrawable) AlitaImage.getBackground(); //define variable

        AlitaAnimation.start(); //begin page load

        Handler myHandler = new Handler();
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() { //load handler
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.alita_anim); //define variable
                AlitaImage.startAnimation(startanimation); // load image / animation
            }
        }, 25);
        AlitaBtn.setOnClickListener(new View.OnClickListener() { // open onclicklistener
            public void onClick(View v) { // open public void
                Intent browserIntent = new Intent( // open internet browser
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/alita-battle-angel-208277/movie-times")); // load uRL
                startActivity(browserIntent); //start browser
                Toast.makeText(AlitaBattleAngel.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); // display message to user
            } // close public void
        }); // close onclicklistener


    } // close void oncreate
} // close public class

