package com.ccacfinalcit135.moviesapplication;


import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class WonderPark extends AppCompatActivity {
    private AnimationDrawable WonderAnimation; //define variable
    private ImageView WonderPImage; //define variable
    private Button Wonderbtn; //define variable

    @Override
    protected void onCreate(Bundle savedInstanceState) { //open void oncreate
        super.onCreate( savedInstanceState ); //create instance
        setContentView( R.layout.activity_wonder_park );  //set activity

        Wonderbtn =   (Button) findViewById( R.id.btnParkID ); //define variable
        WonderPImage = (ImageView) findViewById( R.id.WonderImage ); //define variable

        WonderPImage.setBackgroundResource( R.drawable.wonder_park ); //define background
        WonderAnimation = (AnimationDrawable) WonderPImage.getBackground(); //set background image

        WonderAnimation.start(); //start activity animation

        Handler myHandler = new Handler(); //define handler
        myHandler.postDelayed( new Runnable() {
            @Override
            public void run() { // start handler
                Animation startanimation = AnimationUtils.loadAnimation( getApplicationContext(), R.anim.wonderpark_anim );
                WonderPImage.startAnimation( startanimation ); //start animation
            }
        }, 25 ); //slight delay


        Wonderbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { //open onclicklistener
                Intent browserIntent = new Intent( // set URL
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/wonder-park-212923/movie-times"));
                startActivity(browserIntent); // open browser
                Toast.makeText(WonderPark.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); //display message for user
            }
        }); //close onclicklistener
    } // close void oncreate
}// close public class
