package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class TheLego extends AppCompatActivity { //open public class
    private AnimationDrawable LegoAnimation; //define variable
    private ImageView TheLegoImage; //define variable
    private Button Legobtn; //define variable

    @Override
    protected void onCreate(Bundle savedInstanceState) { //open void oncreate
        super.onCreate( savedInstanceState ); //start instance
        setContentView( R.layout.activity_the_lego ); //set activity
        Legobtn = (Button) findViewById( R.id.btnLegoID ); //define variable

        TheLegoImage = (ImageView)findViewById(R.id.TheLegoImage); //define variable
        TheLegoImage.setBackgroundResource(R.drawable.the_lego); // define background
        LegoAnimation=(AnimationDrawable) TheLegoImage.getBackground(); //set background

        LegoAnimation.start(); // begin animation

        Handler myHandler = new Handler(); //define handler
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() { //start handler
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.lego_anim); //define variable
                TheLegoImage.startAnimation(startanimation); //start animation
            }
        }, 25); //slight delay
        Legobtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { //open onclicklistener
                Intent browserIntent = new Intent( // set browser
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/the-lego-movie-2-the-second-part-185756/movie-times"));
                startActivity(browserIntent); // open broswer
                Toast.makeText(TheLego.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); // display message for user
            }
        }); // close onclicklistner
    }// close void oncreate
} // close public class
