package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class GloriaBell extends AppCompatActivity { // open public class
    private AnimationDrawable GloriaAnimation; //define variable
    private ImageView GloriaBellImage; //define variable
    private Button GloriaBtn; //define variable
    @Override
    protected void onCreate(Bundle savedInstanceState) { // open void oncreate
        super.onCreate( savedInstanceState ); //create instance
        setContentView( R.layout.activity_gloriabell ); // display activity
        GloriaBtn =   (Button) findViewById( R.id.btnGloriaID ); //define variable

        GloriaBellImage = (ImageView)findViewById(R.id.GloriaImage);  //define variable
        GloriaBellImage.setBackgroundResource(R.drawable.gloriabell); //define variable
        GloriaAnimation=(AnimationDrawable) GloriaBellImage.getBackground(); //set background

        GloriaAnimation.start(); //start activity

        Handler myHandler = new Handler(); //create handler
        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() { //start handler
                Animation startanimation = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.alita_anim); //define variable
                GloriaBellImage.startAnimation(startanimation); //start activity
            }
        }, 25); //delay slight
        GloriaBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) { //set onclicklistener
                Intent browserIntent = new Intent( //start browser
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.fandango.com/gloria-bell-215765/movie-times"));
                startActivity(browserIntent); // load browser
                Toast.makeText(GloriaBell.this, "You will move to the www.fandango.com !",
                        Toast.LENGTH_LONG).show(); //display message for user
            }
        });// close onclicklistener
    }//close void oncreate
}//close public class
