package com.ccacfinalcit135.moviesapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ComingSoon extends AppCompatActivity { // open public class

    @Override
    protected void onCreate(Bundle savedInstanceState) { // open oncreate
        super.onCreate( savedInstanceState ); //open instance
        setContentView( R.layout.activity_coming_soon ); //set page as
    } //close oncreate
}//close public class
