package com.ccacfinalcit135.moviesapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity { // open public class
private TextView alitaMovie; //define variable
private TextView FiveMovie; //define variable
private TextView GloriaMovie; //define variable
private TextView DragonMovie; //define variable
private TextView LegoMovie; //define variable
private TextView WonderMovie; //define variable
private TextView DogMovie; //define variable
private TextView JohnMovie; //define variable

    @Override
    protected void onCreate(Bundle savedInstanceState) { // open void oncreate
        super.onCreate(savedInstanceState); //start instance
        setContentView(R.layout.activity_main); //start activity
        alitaMovie=(TextView)findViewById( R.id.Alita ); //define variable
        FiveMovie=(TextView)findViewById( R.id.FiveFeet ); //define variable
        GloriaMovie=(TextView)findViewById( R.id.Gloria ); //define variable
        DragonMovie=(TextView)findViewById( R.id.DragonID ); //define variable
        LegoMovie=(TextView)findViewById( R.id.LegoID ); //define variable
        WonderMovie=(TextView)findViewById( R.id.ParkID ); //define variable
        DogMovie=(TextView)findViewById( R.id.DogID ); //define variable
        JohnMovie=(TextView)findViewById( R.id.JohnID ); //define variable

        alitaMovie.setOnClickListener(new View.OnClickListener(){ //start onclick listener
            @Override
            public void onClick(View v) { // start void onclick
                startActivity(new Intent(MainActivity.this,AlitaBattleAngel.class)); //start activity
            } //close void onclick
        }); //close onclicklistener
        FiveMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { // open void onclick
                startActivity(new Intent(MainActivity.this,FiveFeetApart.class)); //start activity
            } // close void onclick
        });//close onclicklistener

        GloriaMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { //open void onclick
                startActivity(new Intent(MainActivity.this,GloriaBell.class)); //start activity
            } // close void onclick
        });//close onclicklistener

        DragonMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) {//open void onclick
                startActivity(new Intent(MainActivity.this,Dragon.class)); //start activity
            } // close void onclick
        });//close onclicklistener

        LegoMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { //open void onclick
                startActivity(new Intent(MainActivity.this,TheLego.class)); //start activity
            } // close void onclick
        });//close onclicklistener

        WonderMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { //open void onclick
                startActivity(new Intent(MainActivity.this,WonderPark.class)); //start activity
            } // close void onclick
        });//close onclicklistener

        DogMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { //open void onclick
                startActivity(new Intent(MainActivity.this,ComingSoon.class)); //start activity
            } // close void onclick
        }); //close onclicklistener

        JohnMovie.setOnClickListener(new View.OnClickListener(){ //set onclicklistener
            @Override
            public void onClick(View v) { //open void onclick
                startActivity(new Intent(MainActivity.this,ComingSoon.class)); //start activity
            } // close void onclick
        }); //close onclicklistener

    } //close void oncreate
} // close public class
